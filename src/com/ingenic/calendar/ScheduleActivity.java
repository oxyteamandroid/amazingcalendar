/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingCalendar Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.calendar;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.ingenic.calendar.util.CalendarUtil;
import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.datatransactor.elf.ScheduleInfo;

public class ScheduleActivity extends RightScrollActivity {

    private static final int DATE = 0;
    private static final int TIME = 1;

    private ScrollView mScrollView;

    private TextView mTitle;
    private TextView mDateTime;
    private TextView mLocation;
    private TextView mDescription;

    String allday;

    private ScheduleInfo.Event mCalendarInfo;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (CalendarUtil.UPDATE_SCHEDULE.equals(action)) {
                ScheduleActivity.this.finish();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);
        CalendarApplication.getInstance().addActivity(this);

        IntentFilter filter = new IntentFilter(CalendarUtil.UPDATE_SCHEDULE);
        registerReceiver(mReceiver, filter);

        allday = getString(R.string.allday);

        initView();
        Intent intent = getIntent();
        if (intent == null) {
            finish();
        } else {
            mCalendarInfo = (ScheduleInfo.Event) intent
                    .getParcelableExtra("info");
            setDate();
        }
    }

    /**
     * 初始化布局
     */
    private void initView() {
        mScrollView = (ScrollView) findViewById(R.id.scroll_layout);
        mScrollView.setOverScrollMode(View.OVER_SCROLL_NEVER);

        mTitle = (TextView) findViewById(R.id.schedule_title);
        mDateTime = (TextView) findViewById(R.id.schedule_dateTime);
        mLocation = (TextView) findViewById(R.id.schedule_location);
        mDescription = (TextView) findViewById(R.id.schedule_description);
    }

    private void setDate() {
        if (mCalendarInfo == null) {
            return;
        }

        if (!TextUtils.isEmpty(mCalendarInfo.title)) {
            mTitle.setText(mCalendarInfo.title);
            mTitle.setTextColor(0xFF000000);
        }

        String dateTime = getDateTime(mCalendarInfo.dtStart, DATE);
        String dtStart = getDateTime(mCalendarInfo.dtStart, TIME);
        String dtEnd = getDateTime(mCalendarInfo.dtEnd, TIME);

        if (mCalendarInfo.allDay == 1) {
            mDateTime.setText(allday);
            mDateTime.setTextColor(0xFF000000);
        } else if (!TextUtils.isEmpty(dateTime)) {
            mDateTime.setText(dateTime + " " + dtStart + "-" + dtEnd);
            mDateTime.setTextColor(0xFF000000);
        }

        if (!TextUtils.isEmpty(mCalendarInfo.eventLocation)) {
            mLocation.setText(mCalendarInfo.eventLocation);
            mLocation.setTextColor(0xFF000000);
        }

        if (!TextUtils.isEmpty(mCalendarInfo.description)) {
            mDescription.setText(mCalendarInfo.description);
            mDescription.setTextColor(0xFF000000);
        }
    }

    private String getDateTime(long milliseconds, int dt) {
        SimpleDateFormat dateFormat;
        if (dt == DATE) {
            dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        } else {
            dateFormat = new SimpleDateFormat("kk:mm");
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliseconds);
        Date date = calendar.getTime();

        return dateFormat.format(date);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mReceiver != null) {
            unregisterReceiver(mReceiver);
        }
    }
}
