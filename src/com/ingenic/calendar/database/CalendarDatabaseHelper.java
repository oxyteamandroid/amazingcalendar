/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingCalendar Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.calendar.database;

import java.util.ArrayList;
import java.util.Calendar;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;

import com.ingenic.iwds.datatransactor.elf.ScheduleInfo.Event;
import com.ingenic.iwds.utils.IwdsLog;

/**
 * 日历数据库助手
 *
 * @author tZhang
 */
public class CalendarDatabaseHelper {
    private ContentResolver mResolver;
    private Calendar mCalendar;

    public CalendarDatabaseHelper(Context context) {
        mResolver = context.getContentResolver();
        mCalendar = Calendar.getInstance();
    }

    /**
     * 获取所有日程信息
     * @param resolver
     * @return
     */
    public ArrayList<Event> getScheduleForProvider() {
        if (mResolver == null) {
            return null;
        }

        ArrayList<Event> result = new ArrayList<Event>();

        Cursor cursor = mResolver.query(CalendarContrat.Events.CONTENT_URI,
                null, null, null, null);

        if (cursor != null) {
            while (cursor.moveToNext()) {
                buildSchedulInfo(result, cursor);
            }

            cursor.close();
        }

        return result;
    }

    /**
     * 获取某个月的日程信息
     * @param resolver
     * @return
     */
    public ArrayList<Event> getScheduleForProvider(int year, int month) {
        if (mResolver == null) {
            return null;
        }
        ArrayList<Event> result = new ArrayList<Event>();

        Cursor cursor = mResolver.query(CalendarContrat.Events.CONTENT_URI,
                null, null, null, null);

        if (cursor != null) {
            while (cursor.moveToNext()) {
                mCalendar.setTimeInMillis(cursor.getLong(4));

                if (mCalendar.get(Calendar.MONTH) == (month - 1)
                        && mCalendar.get(Calendar.YEAR) == year) {

                    buildSchedulInfo(result, cursor);
                }
            }

            cursor.close();
        }

        return result;
    }

    /**
     * 获取某一天的日程信息
     * @param resolver
     * @return
     */
    public ArrayList<Event> getScheduleForProvider(int year, int month, int day) {
        if (mResolver == null) {
            return null;
        }

        ArrayList<Event> result = new ArrayList<Event>();

        Cursor cursor = mResolver.query(CalendarContrat.Events.CONTENT_URI,
                null, null, null, null);

        if (cursor != null) {
            while (cursor.moveToNext()) {

                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(cursor.getLong(4));

                if (calendar.get(Calendar.DAY_OF_MONTH) == day
                        && calendar.get(Calendar.MONTH) == (month - 1)
                        && calendar.get(Calendar.YEAR) == year) {

                    buildSchedulInfo(result, cursor);
                }

            }

            cursor.close();
        }

        return result;
    }

    /**
     * 获取某个月的日程日期
     * @param resolver
     * @return
     */
    public ArrayList<Integer> getScheduleDateForProvider(int year, int month) {
        if (mResolver == null) {
            return null;
        }

        ArrayList<Integer> dates = null;

        Cursor cursor = mResolver.query(CalendarContrat.Events.CONTENT_URI,
                null, null, null, null);

        if (cursor != null) {
            dates = new ArrayList<Integer>();

            while (cursor.moveToNext()) {

                long date = cursor.getLong(4);
                mCalendar.setTimeInMillis(date);

                if (mCalendar.get(Calendar.MONTH) == (month - 1)
                        && mCalendar.get(Calendar.YEAR) == year) {
                    dates.add(mCalendar.get(Calendar.DAY_OF_MONTH));
                }

            }

            cursor.close();
        }

        return dates;
    }

    /**
     * build 日程
     * @param events
     * @param cursor
     */
    private void buildSchedulInfo(ArrayList<Event> events, Cursor cursor) {
        Event info = new Event();
        info.id = cursor.getLong(0);
        info.title = cursor.getString(1);
        info.eventLocation = cursor.getString(2);
        info.description = cursor.getString(3);
        info.dtStart = cursor.getLong(4);
        info.dtEnd = cursor.getLong(5);
        info.eventTimezone = cursor.getString(6);
        info.duration = cursor.getString(7);
        info.allDay = (int) cursor.getLong(8);
        info.hasAlarm = (int) cursor.getLong(9);
        info.rrule = cursor.getString(10);

        Cursor cursor2 = mResolver.query(CalendarContrat.Reminders.CONTENT_URI,
                null, "event_id=?", new String[] { String.valueOf(info.id) },
                null);

        IwdsLog.d("buildSchedulInfo", "info.reminder : " + info.reminder);

        if (cursor2 != null && cursor2.moveToFirst() && info.reminder != null) {
            info.reminder.minutes = cursor2.getLong(2);
            info.reminder.method = cursor2.getLong(3);
        }

        cursor2.close();

        events.add(info);
    }
}
