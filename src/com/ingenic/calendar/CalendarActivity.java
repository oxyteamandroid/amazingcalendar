/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingCalendar Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.calendar;

import java.util.ArrayList;
import java.util.Calendar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.ingenic.calendar.adapter.CalendarPagerAdapter;
import com.ingenic.calendar.adapter.ScheduleListAdapter;
import com.ingenic.calendar.database.CalendarDatabaseHelper;
import com.ingenic.calendar.util.CalendarUtil;
import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.datatransactor.elf.ScheduleInfo;
import com.ingenic.iwds.widget.AmazingListView;
import com.ingenic.iwds.widget.AmazingToast;
import com.ingenic.iwds.widget.AmazingViewPager;
import com.ingenic.iwds.widget.RightScrollView;

public class CalendarActivity extends RightScrollActivity implements
        AmazingViewPager.OnPageChangedListener {

    /**
     * TAG
     */
    public static final String TAG = "CalendarActivity";

    private static final String ACTION_START_CALENDAR_SERVICE = "com.ingenic.intent.action.START_SYNC_CALENDAR_SERVICE";

    /**
     * 右滑View
     */
    private RightScrollView mContent;

    /**
     * 滑动抽屉
     */
    private DrawerLayout mDrawer;

    // 日期显示控件
    private View mTitleView;
    private TextView mYear;
    private TextView mMonth;

    // 用来存储系统日期
    private int year;
    private int month;

    // 当前显示年份
    private int currentYear;

    // 当前显示月份
    private int currentMonth;

    // 最小可查询年份
    private int startYear;

    // 最大可查询年份
    private int endYear;

    /**
     * 日历上下滑动ViewPager
     */
    private AmazingViewPager mCalendarPager;
    private CalendarPagerAdapter mAdapter;

    /**
     * 显示所有日程的ListView
     */
    private AmazingListView mScheduleList;
    private View emptySchedule;
    private View headView;
    private TextView mScheduleTitle;
    private ScheduleListAdapter mScheduleAdapter;

    /**
     * 日历（日程）的数据库工具类
     */
    private CalendarDatabaseHelper mHelper;

    // 日期格式化
    private String monthFormat;
    private String dateFormat;

    // 跳转Activity的Intent
    private Intent scheduleIntent;
    private Intent datePickerIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        CalendarApplication.getInstance().addActivity(this);

        mContent = getRightScrollView();

        // 确保日程同步的服务正常运行
        Intent intent = new Intent();
        intent.setAction(ACTION_START_CALENDAR_SERVICE);
        sendBroadcast(intent);

        monthFormat = getString(R.string.format_month);
        dateFormat = getString(R.string.format_date);

        // 注册跳转的广播接收者
        if (mReceiver != null) {
            IntentFilter filter = new IntentFilter();
            filter.addAction(CalendarUtil.SKIP_DATE_PAGE);
            filter.addAction(CalendarUtil.UPDATE_CALENDAR);
            filter.addAction(CalendarUtil.UPDATE_SCHEDULE);
            registerReceiver(mReceiver, filter);
        }

        initView();

        setDrawer();

        if (mHelper == null) {
            mHelper = new CalendarDatabaseHelper(this);
        }

        setSchedule();
    }

    /**
     * 初始化布局
     */
    private void initView() {

        // 获取当前年/月/星期
        Calendar calendar = CalendarUtil
                .getCalendar(System.currentTimeMillis());
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH) + 1;

        // 设置最小可查年份
        startYear = CalendarUtil.getCalendarMinYear();

        // 设置最大可查年份
        endYear = CalendarUtil.getCalendarMaxYear();

        // 日期
        mTitleView = findViewById(R.id.date);

        mYear = (TextView) findViewById(R.id.year);
        mYear.setText(year + "");

        mMonth = (TextView) findViewById(R.id.month);
        mMonth.setText(CalendarUtil.getMonth(getResources(), month));

        mCalendarPager = (AmazingViewPager) findViewById(R.id.calendar_pager);
        mCalendarPager.setTouchOrientation(AmazingViewPager.VERTICAL);
        mCalendarPager.setOnPageChangedListener(this);

        if (mAdapter == null) {
            mAdapter = new CalendarPagerAdapter(this);
            mCalendarPager.setAdapter(mAdapter);
            mAdapter.setOnGridItemClickListener(new GridItemClickListener());
        }

        mCalendarPager.setDisplayedChild(computeCurrentPosition(year, month));

        if (datePickerIntent == null) {
            datePickerIntent = new Intent(this, DatePickerActivity.class);
        }

        currentYear = year;
        currentMonth = month;

        // 标题点击事件监听
        mTitleView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!CalendarUtil.isFastDoubleClick()) {
                    // 设置当前界面不能右滑退出
                    if (mContent != null) {
                        mContent.disableRightScroll();
                    }

                    datePickerIntent.putExtra("year", currentYear);
                    datePickerIntent.putExtra("month", currentMonth);
                    startActivity(datePickerIntent);
                }
            }
        });

    }

    /**
     * 设置滑动抽屉
     */
    private void setDrawer() {
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        mDrawer.setDrawerListener(new DrawerLayout.DrawerListener() {

            @Override
            public void onDrawerStateChanged(int arg0) {
            }

            @Override
            public void onDrawerSlide(View arg0, float slideOffset) {
                if (mContent != null) {
                    mContent.disableRightScroll();

                    // 保证在抽屉关闭后右滑可用
                    if (slideOffset == 0) {
                        mContent.enableRightScroll();
                    }
                }

            }

            @Override
            public void onDrawerOpened(View arg0) {
                if (mContent != null) {
                    mContent.disableRightScroll();
                }
            }

            @Override
            public void onDrawerClosed(View arg0) {
                if (mContent != null) {
                    mContent.enableRightScroll();
                }
            }
        });
    }

    /**
     * 设置日程
     */
    private void setSchedule() {
        mScheduleList = (AmazingListView) findViewById(R.id.events);
        emptySchedule = findViewById(R.id.empty_events);
        mScheduleList.setEmptyView(emptySchedule);
        // 设置Item获取焦点
        mScheduleList.setItemsCanFocus(true);

        headView = getLayoutInflater().inflate(R.layout.events_head_layout,
                null);
        mScheduleTitle = (TextView) headView.findViewById(R.id.head);
        mScheduleTitle.setText(String.format(monthFormat, currentYear,
                currentMonth));
        mScheduleList.addHeaderView(headView);

        emptySchedule.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        mScheduleAdapter = new ScheduleListAdapter(CalendarActivity.this);

        // 获取当月日程并设置到日程列表
        new AsyncTask<Void, Void, ArrayList<ScheduleInfo.Event>>() {

            @Override
            protected ArrayList<ScheduleInfo.Event> doInBackground(
                    Void... params) {
                return getScheduleDateFromDataBase(year, month, 0);
            };

            protected void onPostExecute(ArrayList<ScheduleInfo.Event> result) {
                if (mScheduleAdapter != null) {
                    mScheduleAdapter.setDateList(result);
                    mScheduleAdapter.setShowDate(true);
                    mScheduleList.setAdapter(mScheduleAdapter);
                }
            }

        }.execute();

        if (scheduleIntent == null) {
            scheduleIntent = new Intent(CalendarActivity.this,
                    ScheduleActivity.class);
        }
    }

    /**
     * 获取某天或者某月的日程信息
     * 
     * @param year
     * @param month
     * @param day
     */
    private ArrayList<ScheduleInfo.Event> getScheduleDateFromDataBase(int year,
            int month, int day) {

        if (mHelper != null) {
            if (day != 0) { // 查某天
                return mHelper.getScheduleForProvider(year, month, day);
            } else { // 查某月
                return mHelper.getScheduleForProvider(year, month);
            }
        }

        return null;

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mContent != null && mDrawer != null) {
            if (!mDrawer.isDrawerOpen(Gravity.RIGHT)) {
                // 恢复右滑退出功能
                mContent.enableRightScroll();

            } else {
                // 保证不可右滑
                mContent.disableRightScroll();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 注销跳转的广播接收者
        if (mReceiver != null) {
            unregisterReceiver(mReceiver);
        }
    }

    @Override
    public void onBackPressed() {

        if (mDrawer != null && mDrawer.isDrawerVisible(Gravity.RIGHT)) {
            mDrawer.closeDrawers();
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public void onPageChanged(AmazingViewPager pager, int currentPage,
            int oldPage) {

        computeCurrentDate(currentPage);

        mYear.setText(currentYear + "");
        mMonth.setText(CalendarUtil.getMonth(getResources(), currentMonth));

        // 启动异步线程去获取这一个月的日程信息
        new AsyncTask<Void, Void, ArrayList<ScheduleInfo.Event>>() {

            @Override
            protected ArrayList<ScheduleInfo.Event> doInBackground(
                    Void... params) {
                return getScheduleDateFromDataBase(currentYear, currentMonth, 0);
            };

            protected void onPostExecute(ArrayList<ScheduleInfo.Event> result) {
                if (mScheduleAdapter != null) {
                    mScheduleTitle.setText(String.format(monthFormat,
                            currentYear, currentMonth));
                    mScheduleAdapter.setDateList(result);
                    mScheduleAdapter.setShowDate(true);
                    mScheduleAdapter.notifyDataSetChanged();
                }
            }

        }.execute();
    }

    /**
     * 根据position计算年份和月份
     */
    private void computeCurrentDate(int position) {
        currentYear = startYear + position / 12;
        currentMonth = position % 12 + 1;
    }

    /**
     * 根据年份和月份计算position
     */
    private int computeCurrentPosition(int y, int m) {

        if (y < startYear || y > endYear || m < 1 || m > 12) {
            return computeCurrentPosition(year, month);
        }

        return 12 * (y - startYear) + m - 1;
    }

    /**
     * 跳转到指定日期的广播接收者
     */
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            if (CalendarUtil.SKIP_DATE_PAGE.equals(action)) {
                if (mCalendarPager != null) {
                    int y = intent.getIntExtra("year", -1);
                    int m = intent.getIntExtra("month", -1);

                    // 跳转到指定日期
                    mCalendarPager.setDisplayedChild(computeCurrentPosition(y,
                            m));
                }
            } else if (CalendarUtil.UPDATE_CALENDAR.equals(action)) { // 断开连接
                UpdateCalendar();
                AmazingToast.showToast(CalendarActivity.this,
                        R.string.disconnected, AmazingToast.LENGTH_SHORT,
                        AmazingToast.BOTTOM_CENTER);
                mDrawer.closeDrawer(Gravity.RIGHT);
            } else if (CalendarUtil.UPDATE_SCHEDULE.equals(action)) { // 更新日程列表
                UpdateCalendar();
                AmazingToast.showToast(CalendarActivity.this, R.string.update,
                        AmazingToast.LENGTH_SHORT, AmazingToast.BOTTOM_CENTER);
            }
        }

    };

    /**
     * 更新日历
     */
    private void UpdateCalendar() {

        ArrayList<ScheduleInfo.Event> mDatas = getScheduleDateFromDataBase(
                currentYear, currentMonth, 0);

        mAdapter.notifyDataSetChanged();

        mScheduleTitle.setText(String.format(monthFormat, currentYear,
                currentMonth));

        if (mScheduleAdapter != null) {
            mScheduleAdapter.setDateList(mDatas);
            mScheduleAdapter.setShowDate(true);
            mScheduleAdapter.notifyDataSetChanged();
        }

        if (mDatas.size() == 0 && mDrawer != null
                && mDrawer.isDrawerOpen(Gravity.RIGHT)) {
            mDrawer.closeDrawer(Gravity.RIGHT);
        }
    }

    /**
     * GridView Item点击监听
     * 
     * @author tzhang
     * 
     */
    private class GridItemClickListener implements
            AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                long id) {
            final int day = Integer.parseInt(((TextView) view).getText()
                    .toString());

            new AsyncTask<Void, Void, ArrayList<ScheduleInfo.Event>>() {

                @Override
                protected ArrayList<ScheduleInfo.Event> doInBackground(
                        Void... params) {
                    // 获取点击的日期日程信息
                    return getScheduleDateFromDataBase(currentYear,
                            currentMonth, day);
                }

                @Override
                protected void onPostExecute(
                        ArrayList<ScheduleInfo.Event> result) {
                    super.onPostExecute(result);
                    if (result.size() == 0) {
                        // 无日程
                        AmazingToast.showToast(CalendarActivity.this,
                                getString(R.string.no_schedule),
                                AmazingToast.LENGTH_SHORT,
                                AmazingToast.BOTTOM_CENTER);
                    } else {
                        // 更新日程列表
                        if (mScheduleAdapter != null) {
                            mScheduleTitle.setText(String.format(dateFormat,
                                    currentYear, currentMonth, day));
                            mScheduleAdapter.setDateList(result);
                            mScheduleAdapter.setShowDate(false);
                            mScheduleAdapter.notifyDataSetChanged();
                        }

                        // 打开日程列表
                        mDrawer.openDrawer(Gravity.RIGHT);
                    }
                }

            }.execute();

        }

    }

}
