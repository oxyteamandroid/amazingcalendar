/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingCalendar Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.calendar.util;

import android.content.res.Resources;

import com.ingenic.calendar.R;

import java.util.Calendar;

/**
 * 日历工具类
 *
 * @author tZhang
 */
public class CalendarUtil {

    /**
     * 跳转到指定日期的ACTION
     */
    public static final String SKIP_DATE_PAGE = "com.ingenic.action.SKIP_DATE_PAGE";

    /**
     * 连接手机时用到的UUID
     */
    public static final String UUID = "3b11571f-64eb-4085-1e5e-50fb6f0aff83";

    /**
     * 更新日历的广播ACTION
     */
    public static final String UPDATE_CALENDAR = "com.ingenic.action.UPDATE_CALENDAR";

    /**
     * 更新日程的广播ACTION
     */
    public static final String UPDATE_SCHEDULE = "com.ingenic.action.UPDATE_SCHEDULE";

    /**
     * 日期对象
     */
    private static Calendar calendar = Calendar.getInstance();

    /**
     * 获取对应时间的日期对象
     * @param milliseconds
     *            毫秒数
     * @return
     */
    public static Calendar getCalendar(long milliseconds) {
        calendar.setTimeInMillis(milliseconds);
        return calendar;
    }

    private static int[] date = new int[3];

    /**
     * 获取系统当前日期
     * @return 年月日
     */
    public static int[] getSystemDate() {
        calendar.setTimeInMillis(System.currentTimeMillis());
        date[0] = calendar.get(Calendar.YEAR);
        date[1] = calendar.get(Calendar.MONTH) + 1;
        date[2] = calendar.get(Calendar.DAY_OF_MONTH);

        return date;
    }

    /**
     * 获取最小可查年份
     * @return
     */
    public static int getCalendarMinYear() {
        return getCalendar(System.currentTimeMillis()).get(Calendar.YEAR) - 49;
    }

    /**
     * 获取最大可查年份
     * @return
     */
    public static int getCalendarMaxYear() {
        return getCalendar(System.currentTimeMillis()).get(Calendar.YEAR) + 50;
    }

    private static long lastClickTime;

    /**
     * 一秒内多次点击
     * @return
     */
    public static boolean isFastDoubleClick() {
        long time = System.currentTimeMillis();
        long timeD = time - lastClickTime;
        if (0 < timeD && timeD < 1000) {
            return true;
        }
        lastClickTime = time;
        return false;
    }

    /**
     * 判断是否为闰年
     * @param year
     * @return
     */
    public static boolean isLeapYear(int year) {
        if (year % 100 == 0 && year % 400 == 0) {
            return true;
        } else if (year % 100 != 0 && year % 4 == 0) {
            return true;
        }
        return false;
    }

    /**
     * 得到某月有多少天数
     * @param isLeapyear
     *            是否为闰年
     * @param month
     *            月份
     * @return
     */
    public static int getDaysOfMonth(boolean isLeapyear, int month) {
        if (month < 1 || month > 12) {
            return 0;
        }

        switch (month) {
        case 2:
            if (isLeapyear) {
                return 29;
            } else {
                return 28;
            }
        case 4:
        case 6:
        case 9:
        case 11:
            return 30;
        default:
            return 31;
        }
    }

    /**
     * 指定某年中的某月的第一天是星期几
     * @param year
     *            年份
     * @param month
     *            月份
     * @return
     */
    public static int getWeekdayOfMonth(int year, int month) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month - 1, 1);
        return cal.get(Calendar.DAY_OF_WEEK) - 1;
    }

    /**
     * 获取月份的国际化字符串
     * @param resources
     * @param month
     * @return
     */
    public static String getMonth(Resources resources, int month) {
        String[] months = resources.getStringArray(R.array.month_entries);
        if (month <= 0 || month > months.length) {
            return months[0];
        } else {
            return months[month - 1];
        }
    }

    /**
     * 获取星期的国际化字符串
     * @param dayOfWeek
     * @return
     */
    public static String getDayOfWeek(Resources resources, int dayOfWeek) {
        String[] weeks = resources.getStringArray(R.array.day_of_week_entries);
        if (dayOfWeek <= 0 || dayOfWeek > weeks.length) {
            return weeks[0];
        } else {
            return weeks[dayOfWeek - 1];
        }
    }
}
