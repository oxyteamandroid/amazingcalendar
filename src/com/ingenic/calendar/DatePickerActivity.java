/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingCalendar Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.calendar;

import java.util.Calendar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.ingenic.calendar.util.CalendarUtil;
import com.ingenic.iwds.app.RightScrollActivity;

public class DatePickerActivity extends RightScrollActivity implements
        NumberPicker.OnValueChangeListener, View.OnClickListener {

    // 年月日NumberPicker
    private NumberPicker yearPicker;
    private NumberPicker monthPicker;
    private NumberPicker dayPicker;

    // 当前系统日期
    private int sysYear;
    private int sysMonth;
    private int sysDay;

    // NumberPicker指示的年月日值
    private int year;
    private int month;
    private int day;

    // 可查年份
    private int maxYear;
    private int minYear;

    // 当前只是月份的天数
    private int maxDay;

    private Calendar mCalendar = Calendar.getInstance();

    // 当前指示日期
    private TextView mDate;
    private String mDateStr;

    // 按钮
    private Button mSetToday; // 重置到今天
    private Button mSetDate; // 跳转到当前指示的日期

    // 跳转到日历界面的Intent
    private Intent mIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datepicker);
        CalendarApplication.getInstance().addActivity(this);
        init();
    }

    private void init() {

        // 获取系统日期
        int[] sysDate = CalendarUtil.getSystemDate();
        sysYear = sysDate[0];
        sysMonth = sysDate[1];
        sysDay = sysDate[2];

        // 初始化当前指示日期
        Intent intent = getIntent();
        if (intent != null) {
            year = intent.getIntExtra("year", sysYear);
            month = intent.getIntExtra("month", sysMonth);
        } else {
            year = sysYear;
            month = sysMonth;
        }

        day = sysDay;

        // 防止越界(例如出现2月30日)
        maxDay = getDay(year, month);
        if (maxDay < day) {
            day = maxDay;
        }

        // 初始化可查年份
        maxYear = year + 50;
        minYear = year - 49;

        // 初始化控件
        mDate = (TextView) findViewById(R.id.selected_date);

        yearPicker = (NumberPicker) findViewById(R.id.year);
        monthPicker = (NumberPicker) findViewById(R.id.month);
        dayPicker = (NumberPicker) findViewById(R.id.day);

        mSetToday = (Button) findViewById(R.id.reset);
        mSetDate = (Button) findViewById(R.id.confirm);

        // 设置指示日期的格式
        mDateStr = getString(R.string.date_format);

        // 设置其不可获取焦点，即使设置了可获取焦点也会无效
        yearPicker
                .setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        monthPicker
                .setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        dayPicker
                .setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        // 初始化值
        yearPicker.setMaxValue(maxYear);
        yearPicker.setMinValue(minYear);
        yearPicker.setValue(year);

        monthPicker.setMaxValue(12);
        monthPicker.setMinValue(1);
        monthPicker.setValue(month);

        dayPicker.setMaxValue(maxDay);
        dayPicker.setMinValue(1);
        dayPicker.setValue(day);

        // 设置值改变的监听
        yearPicker.setOnValueChangedListener(this);
        monthPicker.setOnValueChangedListener(this);
        dayPicker.setOnValueChangedListener(this);

        mSetToday.setOnClickListener(this);
        mSetDate.setOnClickListener(this);

        // 初始化显示时间
        setDate();

        mIntent = new Intent(CalendarUtil.SKIP_DATE_PAGE);
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        int id = picker.getId();

        switch (id) {
        case R.id.year:
            year = newVal;
            dayPicker.setMaxValue(getDay(year, month));
            break;

        case R.id.month:
            month = newVal;
            dayPicker.setMaxValue(getDay(year, month));
            break;

        case R.id.day:
            day = newVal;
            break;

        default:
            break;
        }

        setDate();
    }

    /**
     * 根据年份和月份计算这个月的天数
     * 
     * @param year
     * @param month
     * @return
     */
    private int getDay(int year, int month) {
        int day;
        if (year % 4 == 0 && year % 100 != 0) { // 闰年
            if (month == 1 || month == 3 || month == 5 || month == 7
                    || month == 8 || month == 10 || month == 12) {
                day = 31;
            } else if (month == 2) {
                day = 29;
            } else {
                day = 30;
            }
        } else { // 平年
            if (month == 1 || month == 3 || month == 5 || month == 7
                    || month == 8 || month == 10 || month == 12) {
                day = 31;
            } else if (month == 2) {
                day = 28;
            } else {
                day = 30;
            }
        }
        return day;
    }

    /**
     * 设置显示日期
     */
    private void setDate() {
        mCalendar.set(year, month - 1, day);
        int number = mCalendar.get(Calendar.DAY_OF_WEEK);
        mDate.setText(String.format(mDateStr, year,
                CalendarUtil.getMonth(getResources(), month), day,
                CalendarUtil.getDayOfWeek(getResources(), number)));
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
        case R.id.reset:
            year = sysYear;
            month = sysMonth;
            day = sysDay;
            yearPicker.setValue(sysYear);
            monthPicker.setValue(sysMonth);
            dayPicker.setMaxValue(getDay(sysYear, sysMonth));
            dayPicker.setValue(sysDay);
            setDate();
            break;

        case R.id.confirm:
            if (mIntent != null) {
                mIntent.putExtra("year", year);
                mIntent.putExtra("month", month);
                sendBroadcast(mIntent);

                this.finish();
            }
            break;

        default:
            break;
        }

    }

}
