/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingCalendar Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.calendar.adapter;

import java.util.ArrayList;
import java.util.Calendar;

import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ingenic.calendar.R;
import com.ingenic.calendar.database.CalendarDatabaseHelper;
import com.ingenic.calendar.util.CalendarUtil;

public class CalendarGridAdapter extends BaseAdapter {

    private Resources mResources;

    private LayoutInflater mInflater;

    /** 系统年月日 */
    private int systemYear;
    private int systemMonth;
    private int systemDay;

    private boolean isLeapyear = false; // 是否为闰年
    private int daysOfMonth = 0; // 某月的天数
    private int dayOfWeek = 0; // 具体某一天是星期几
    private int lastDaysOfMonth = 0; // 上一个月的总天数

    private int currentFlag = -1; // 用于标记当天

    /** 存储每一天的日期 */
    private String[] dayNumber = new String[42];

    private ArrayList<Integer> schedules;
    private ArrayList<Integer> schedulePistions = new ArrayList<Integer>();

    private CalendarDatabaseHelper mHelper;

    public CalendarGridAdapter(Context context) {
        mResources = context.getResources();
        mInflater = LayoutInflater.from(context);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        systemYear = calendar.get(Calendar.YEAR);
        systemMonth = calendar.get(Calendar.MONTH) + 1;
        systemDay = calendar.get(Calendar.DAY_OF_MONTH);

        mHelper = new CalendarDatabaseHelper(context);
    }

    public void setTime(final int year, final int month) {

        getCalendar(year, month);

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                // 获取当月的日程信息
                schedules = mHelper.getScheduleDateForProvider(year, month);
                return null;
            };

            protected void onPostExecute(Void result) {
                getCalendar(year, month);
                notifyDataSetChanged();
            }

        }.execute();
    }

    @Override
    public int getCount() {
        return dayNumber.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.calendar_grid_item, null);
        }

        // 设置日期
        ((TextView) convertView).setText(dayNumber[position]);

        if (position < daysOfMonth + dayOfWeek && position >= dayOfWeek) {
            if (currentFlag == position) {
                // 设置当天的字体设红色
                ((TextView) convertView).setTextColor(mResources
                        .getColorStateList(R.color.today_grid_text_selector));
                ((TextView) convertView)
                        .setBackgroundResource(R.drawable.grid_item_selector);
            } else {
                // 当前月其他字体设白色
                ((TextView) convertView).setTextColor(mResources
                        .getColorStateList(R.color.simple_grid_text_selector));
            }

            // 标记日程
            if (schedulePistions != null) {
                for (int i = 0; i < schedulePistions.size(); i++) {
                    if (position == schedulePistions.get(i)) {
                        ((TextView) convertView)
                                .setBackgroundResource(R.drawable.grid_schedule_item_selector);
                        break;
                    }
                }
            }
        } else {
            // 其他月份字体设灰色
            ((TextView) convertView).setTextColor(mResources
                    .getColorStateList(R.color.other_grid_text_selector));
        }

        return convertView;
    }

    // 得到某年的某月的天数且这月的第一天是星期几
    public void getCalendar(int year, int month) {
        isLeapyear = CalendarUtil.isLeapYear(year); // 是否为闰年
        daysOfMonth = CalendarUtil.getDaysOfMonth(isLeapyear, month); // 某月的总天数
        dayOfWeek = CalendarUtil.getWeekdayOfMonth(year, month); // 某月第一天为星期几
        lastDaysOfMonth = month == 1 ? 31 : (CalendarUtil.getDaysOfMonth(isLeapyear, month - 1)); // 上一个月的总天数(如果当前是１月，那么上一个月就是去年的12月，每年12月为31天)
        getweek(year, month);
    }

    // 将一个月中的每一天的值添加入数组dayNumber中
    private void getweek(int year, int month) {
        schedulePistions.clear();

        int j = 1;
        int day;
        // 得到当前月的所有日程日期(这些日期需要标记)
        for (int i = 0; i < dayNumber.length; i++) {
            if (i < dayOfWeek) { // 前一个月
                int temp = lastDaysOfMonth - dayOfWeek + 1;
                day = temp + i;

            } else if (i < daysOfMonth + dayOfWeek) { // 本月
                day = i - dayOfWeek + 1; // 得到的日期
                // 标记当天日期
                if (systemYear == year && systemMonth == month
                        && systemDay == day) {
                    // 标记当前日期
                    currentFlag = i;
                }

            } else { // 下一个月
                day = j;
                j++;
            }

            dayNumber[i] = day + "";

            if (schedules != null) {
                for (int k = 0; k < schedules.size(); k++) {
                    if (schedules.get(k) == day) {
                        schedulePistions.add(i);
                        break;
                    }
                }
            }

        }
    }

}
