/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingCalendar Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.calendar.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ingenic.calendar.R;
import com.ingenic.calendar.ScheduleActivity;
import com.ingenic.iwds.datatransactor.elf.ScheduleInfo;

public class ScheduleListAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<ScheduleInfo.Event> mList;
    private LayoutInflater mInflater;
    private boolean mShowDate = true;

    private Intent scheduleIntent;

    public ScheduleListAdapter(Context context) {
        mContext = context;
        mInflater = LayoutInflater.from(context);

        scheduleIntent = new Intent(context, ScheduleActivity.class);
    }

    /**
     * 设置日程数据
     *
     * @param list
     */
    public void setDateList(ArrayList<ScheduleInfo.Event> list) {
        mList = list;
    }

    /**
     * 设置是否显示日期（月-日）
     */
    public void setShowDate(boolean showDate) {
        mShowDate = showDate;
    }

    @Override
    public int getCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.events_item_layout, null);
            holder = new ViewHolder();
            holder.eventTime = (TextView) convertView.findViewById(R.id.time);
            holder.eventTitle = (TextView) convertView.findViewById(R.id.title);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        SimpleDateFormat dateFormat;
        if (mShowDate) {
            dateFormat = new SimpleDateFormat("M-d\nkk : mm");
        } else {
            dateFormat = new SimpleDateFormat("kk : mm");
        }

        final ScheduleInfo.Event info = mList.get(position);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(info.dtStart);
        Date date = calendar.getTime();

        holder.eventTime.setText(dateFormat.format(date));

        String title = info.title;
        if (TextUtils.isEmpty(title)) {
            holder.eventTitle.setText(R.string.schedule_no_title);
        } else {
            holder.eventTitle.setText(title);
        }

        convertView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                scheduleIntent.putExtra("info", info);
                mContext.startActivity(scheduleIntent);
            }
        });

        return convertView;
    }

    private class ViewHolder {
        TextView eventTime;
        TextView eventTitle;
    }

}
