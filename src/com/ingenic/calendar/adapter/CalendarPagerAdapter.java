/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingCalendar Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.calendar.adapter;

import java.util.Calendar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;

import com.ingenic.calendar.R;
import com.ingenic.calendar.util.CalendarUtil;

public class CalendarPagerAdapter extends BaseAdapter {

    private Context mContext;

    private LayoutInflater mInflater;

    private CalendarGridAdapter mGridAdapter;

    // 当前年份
    private int currentYear;

    // 最小可查询年份
    private int startYear;

    // 当前月份(最小月份)
    private int currentMonth;

    // GridView的布局动画
    private LayoutAnimationController mController;

    private int year;
    private int month;

    private boolean isFirst = false;

    public CalendarPagerAdapter(Context context) {
        mContext = context;
        mInflater = LayoutInflater.from(context);

        // 设置进入动画
        mController = AnimationUtils.loadLayoutAnimation(context,
                R.anim.grid_item_slide_right);

        // 记录当前年月
        Calendar calendar = CalendarUtil
                .getCalendar(System.currentTimeMillis());
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH) + 1;

        // 设置最小可查年份
        startYear = CalendarUtil.getCalendarMinYear();
    }

    @Override
    public int getCount() {
        // 100年 * 12个月 （前后50年，包括今年，共100年）
        return 100 * 12;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater
                    .inflate(R.layout.calendar_page_layout, null);
        }

        computeCurrentDate(position);

        GridView mGridView = (GridView) convertView
                .findViewById(R.id.grid_page);
        // 去除阴影
        mGridView.setOverScrollMode(View.OVER_SCROLL_NEVER);

        mGridAdapter = new CalendarGridAdapter(mContext);
        mGridAdapter.setTime(currentYear, currentMonth);
        mGridView.setAdapter(mGridAdapter);

        if (!isFirst && currentYear == year && currentMonth == month) {
            isFirst = true;
            mGridView.setLayoutAnimation(mController);
        }

        if (mListener != null) {
            mGridView.setOnItemClickListener(mListener);
        }

        return convertView;
    }

    /**
     * 根据position计算当前年份和月份
     */
    private void computeCurrentDate(int position) {
        currentYear = startYear + position / 12;
        currentMonth = position % 12 + 1;
    }

    public int getCurrentYear() {
        return currentYear;
    }

    public void setCurrentYear(int currentYear) {
        this.currentYear = currentYear;
    }

    public int getCurrentMonth() {
        return currentMonth;
    }

    public void setCurrentMonth(int currentMonth) {
        this.currentMonth = currentMonth;
    }

    private OnItemClickListener mListener;

    public void setOnGridItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

}
