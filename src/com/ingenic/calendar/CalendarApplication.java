/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingCalendar/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.calendar;

import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/**
 * CalendarApplication
 *
 * @author tZhang
 */

public class CalendarApplication extends Application {

    private List<Activity> mList = new LinkedList<Activity>();
    private static CalendarApplication mInstance;

    private BroadcastReceiver mReceiver;
    private IntentFilter mFilter;

    @Override
    public void onCreate() {
        super.onCreate();
        if (null == mFilter) {
            mFilter = new IntentFilter();
            mFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
            mFilter.addAction(Intent.ACTION_TIME_CHANGED);
            mFilter.addAction(Intent.ACTION_DATE_CHANGED);
        }

        if (null == mReceiver) {
            mReceiver = new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {
                    // 监听到日期/时区改变时退出程序
                    exit();
                }
            };
        }

        registerReceiver(mReceiver, mFilter);
    }

    public synchronized static CalendarApplication getInstance() {
        if (null == mInstance) {
            mInstance = new CalendarApplication();
        }
        return mInstance;
    }

    /**
     * Add Activity
     *
     * @param activity
     */
    public void addActivity(Activity activity) {
        mList.add(activity);
    }

    /**
     * 退出应用程序
     */
    public void exit() {
        try {
            for (Activity activity : mList) {
                if (activity != null)
                    activity.finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mReceiver != null) {
                unregisterReceiver(mReceiver);
            }

            System.exit(0);
        }
    }
}
